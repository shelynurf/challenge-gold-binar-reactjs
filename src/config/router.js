import { useRoutes } from "react-router-dom";
import Home from "../pages/home";
import SearchCar from "../pages/search-car";
import DetailCar from "../pages/search-car/detail";
import Payment from "../pages/pembayaran/payment";
import PaymentConfirm from "../pages/pembayaran/payment-confirm";
import Ticket from "../pages/pembayaran/ticket";
import Login from "../pages/login";
import Register from "../pages/login/register";

const publicRoutes = (props) => {
	return [
		{ index: true, path: "/", element: <Home {...props} title="Home" /> },
		{
			index: true,
			path: "/cari-mobil",
			element: <SearchCar {...props} title="Cari Mobil" />,
		},
		{
			index: true,
			path: "/login",
			element: <Login {...props} title="Login" />,
		},

		{
			index: true,
			path: "/cari-mobil/:id",
			element: <DetailCar {...props} title="Cari Mobil" />,
		},
		{
			index: true,
			path: "/payment/:id",
			element: <Payment {...props} title="Pembayaran" />,
		},
		{
			index: true,
			path: "/payment/confirm/:id",
			element: <PaymentConfirm {...props} title="Metode Pembayaran" />,
		},
		{
			index: true,
			path: "/payment/ticket/:id",
			element: <Ticket {...props} title="Tiket" />,
		},
		{ index: true, path: "*", element: <div>Halaman Not Found</div> },
	];
};

const PublicRoutes = (props) => {
	const routes = useRoutes(publicRoutes(props));
	return routes;
};

export default PublicRoutes;
