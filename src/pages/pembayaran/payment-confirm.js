import React, { useEffect, useState } from "react";
import Button from "../../component/button";
import PaymentNav from "../../component/paymentNav";
import {
	Navigate,
	useLocation,
	useNavigate,
	useParams,
} from "react-router-dom";
import "react-dropzone-uploader/dist/styles.css";
import { FileUploader } from "react-drag-drop-files";
import { Input } from "reactstrap";
import { Toast } from "react-bootstrap";
import useCountdown from "../../component/countdown";
import CountdownTimer from "../../component/countdownTimer";
import { useDispatch, useSelector } from "react-redux";
import { Popover } from "@mui/material";
import { v4 as uuid } from "uuid";
import { Token } from "../../config/token";

const fileTypes = ["JPG", "PNG", "GIF"];

const PaymentConfirm = (props) => {
	const navigate = useNavigate();
	const getUploadParams = ({ meta }) => {
		return { url: "https://httpbin.org/post" };
	};
	const handleChangeStatus = ({ meta, file }, status) => {
		console.log(status, meta, file);
	};

	const handleSubmit = (files, allFiles) => {
		// console.log(files.map((f) => f.meta));
		allFiles.forEach((f) => f.remove());
	};

	const [file, setFile] = useState(null);
	const [disabled, setDisabled] = useState(true);
	const getBase64 = (file, functions) => {
		if (file) {
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				setFile(reader.result);
			};
			reader.onerror = function (error) {
				return "Error : ";
			};
		}
	};
	const handleChange = (file) => {
		getBase64(file);
		setDisabled(false);
	};

	const copyText = (text) => {
		navigator.clipboard.writeText(document.getElementById(text).value);
		// const [anchorEl, setAnchorEl] = useState(null);

		// const handleClick = (event) => {
		// 	setAnchorEl(event.currentTarget);
		// };

		// const handleClose = () => {
		// 	setAnchorEl(null);
		// };

		// const open = Boolean(anchorEl);
		// const id = open ? "simple-popover" : undefined;
		alert("Text Successfully Copied");
		// return (
		// 	<Popover
		// 		id={id}
		// 		open={open}
		// 		anchorEl={anchorEl}
		// 		onClose={handleClose}
		// 		anchorOrigin={{
		// 			vertical: "bottom",
		// 			horizontal: "left",
		// 		}}></Popover>
		// );
	};

	const [confirm, setConfirm] = useState(false);
	const handleClick = () => {
		setConfirm(true);
	};
	// console.log("confirm : ", confirm);
	const { id } = useParams();

	const handleClick2 = (params) => {
		setSelectBayar(params);
	};

	// const { state } = useLocation();
	const passingBank = useSelector((state) => state?.stepper?.konfirmasi);
	const [selectBayar, setSelectBayar] = useState(
		`${passingBank?.metodeBayar[0]?.metode}`
	);

	const invNumber = () => {
		const invRandom = () => {
			return Math.floor(Math.random() * 100000);
		};
		const invUUID = () => {
			return uuid().slice(0, 7);
		};
		const date = new Date().toJSON().slice(0, 10).replace(/-/g, "");
		// console.log("typeof invRandom", typeof invRandom);
		return `${invRandom()} / ${invUUID()} / ${date};`;
	};

	const dispatch = useDispatch();
	useEffect(() => {
		dispatch({
			type: "upload_bukti",
			payload: ["pilih-selesai", "upload"],
		});
	}, [dispatch]);

	useEffect(() => {
		dispatch({
			type: "konfirmasi",
			payload: passingBank,
		});
	}, [dispatch]);

	const today = new Date();
	const paymentDeadline = new Date(today);
	paymentDeadline.setDate(paymentDeadline.getDate() + 1);

	const totalPrice = useSelector((state) => state?.orderDate?.totalPrice);
	const formatNumber = (number) =>
		new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		})
			.format(number)
			.slice(0, -3);

	if (!Token) return <Navigate to={"/"} />;

	return (
		<div>
			<div style={{ height: "64px", backgroundColor: "#f1f3ff" }}>
				<PaymentNav payNav={`${passingBank?.name} Transfer`} />
			</div>

			<div className="mx-10 mt-5">
				<div className="row">
					<div className="col-md pe-5">
						<div className="row pb-4">
							<div className="card-car d-flex justify-content-between align-items-center px-4">
								<div>
									<p className="font-14">Selesaikan Pembayaran Sebelum</p>
									<p className="font-14-thin mb-0">
										{`${Intl.DateTimeFormat("in-ID", {
											year: "numeric",
											month: "long",
											day: "numeric",
											weekday: "long",
											hour: "numeric",
											minute: "numeric",
										}).format(paymentDeadline)} `}
										WIB
									</p>
								</div>

								<CountdownTimer
									targetDate={
										new Date().getTime() + 24 * 60 * 60 * 1000
									}></CountdownTimer>
							</div>
						</div>
						<div className="row pb-4">
							<div className="card-car px-4 d-grid">
								<div>
									<p className="font-14">Lakukan Transfer Ke</p>
									<div className="d-flex gap-3 align-items-start font-12">
										<img
											src={passingBank?.bankImg}
											alt="bankImg"
											className="bank px-1 py-2"
										/>
										<div>
											<p className="mb-0">{passingBank?.name} Transfer</p>
											<p>a.n Binar Car Rental</p>
										</div>
									</div>
								</div>

								<div>
									<label
										className="font-12 mb-1"
										style={{ fontWeight: "300", color: "#3C3C3C" }}>
										Nomor Rekening
									</label>
									<div className="d-flex">
										<input
											type="text"
											value={54104257877}
											id="rekening"
											className="d-flex text-copy font-12 mb-0 bg-white"
											style={{ width: "100%", borderRight: "none" }}
											disabled></input>
										<div
											className="d-flex align-items-center pe-2"
											style={{
												border: "1px solid black",
												borderLeft: "none",
											}}>
											<span
												className="material-icons"
												style={{ cursor: "pointer" }}
												onClick={() => copyText("rekening")}>
												content_copy
											</span>
										</div>
									</div>
								</div>
								<div>
									<label
										className="font-12 mb-1"
										style={{ fontWeight: "300", color: "#3C3C3C" }}>
										Total Bayar
									</label>
									<div className="d-flex">
										<input
											type="text"
											value={formatNumber(totalPrice)}
											id="totalBayar"
											className="d-flex text-copy font-12 mb-0 bg-white"
											style={{
												fontWeight: "700",
												width: "100%",
												borderRight: "none",
											}}
											disabled></input>
										<div
											className="d-flex align-items-center pe-2"
											style={{
												border: "1px solid black",
												borderLeft: "none",
											}}>
											<span
												className="material-icons"
												style={{ cursor: "pointer" }}
												onClick={() => copyText("totalBayar")}>
												content_copy
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="row pb-4">
							<div className="card-car px-4 d-grid gap-4 pb-5">
								<p className="font-14">Intruksi Pembayaran</p>

								<div className="row font-14">
									{passingBank?.metodeBayar?.map((item, index) => {
										return (
											<div className="col text-center">
												{item.metode === selectBayar ? (
													<p
														style={{ cursor: "pointer" }}
														onClick={() => handleClick2(item.metode)}>
														{item.metode}
													</p>
												) : (
													<p
														style={{ cursor: "pointer" }}
														className="text-p"
														onClick={() => handleClick2(item.metode)}>
														{item.metode}
													</p>
												)}

												{item.metode === selectBayar ? (
													<hr style={{ color: "#5CB85F" }}></hr>
												) : (
													<hr style={{ color: "#EEEEEE" }}></hr>
												)}
											</div>
										);
									})}
								</div>

								<ul
									className="row text-p"
									style={{
										listStyle: "disc",
										color: "#8A8A8A",
										gap: "0.8rem",
										paddingBottom: "2rem",
									}}>
									{passingBank?.metodeBayar?.map((item, index) => {
										return (
											item.metode === selectBayar &&
											item.instruksi?.map((data, index) => {
												return <li>{data}</li>;
											})
										);
									})}
								</ul>
							</div>
						</div>
					</div>
					<div className="col-md-5">
						<div className="card-car px-4 d-grid gap-5">
							{!confirm ? (
								<div>
									<p className="font-14-thin text-start">
										Klik konfirmasi pembayaran untuk mempercepat proses
										pengecekan
									</p>
									<Button
										className="btn-sewa-mobil font-14 w-100"
										onClick={() => handleClick()}>
										Konfirmasi Pembayaran
									</Button>
								</div>
							) : (
								<div className="d-grid gap-3">
									<div>
										<div className="d-flex justify-content-between">
											<p className="font-14">Konfirmasi Pembayaran</p>
											{/* <p className="bg-danger">countdown</p> */}
											<CountdownTimer
												targetDate={
													new Date().getTime() + 10 * 60 * 1000
												}></CountdownTimer>
										</div>
										<p className="font-14-thin">
											Terima kasih telah melakukan konfirmasi pembayaran.
											Pembayaranmu akan segera kami cek tunggu kurang lebih 10
											menit untuk mendapatkan konfirmasi.
										</p>
									</div>
									<div>
										<p className="font-14">Upload Bukti Pembayaran</p>
										<p className="font-14-thin">
											Untuk membantu kami lebih cepat melakukan pengecekan. Kamu
											bisa upload bukti bayarmu
										</p>
									</div>

									<div className="d-flex justify-content-center">
										<FileUploader
											handleChange={handleChange}
											name="file"
											types={fileTypes}
											hoverTitle="Drop Here"
											children={
												<div
													className="d-flex align-items-center justify-content-center"
													style={{
														backgroundColor: "#EEEEEE",
														border: "1px dashed #D0D0D0",
														borderRadius: "4p",
														height: "162px",
														width: "296px",
														cursor: "pointer",
													}}>
													{!file && <span class="material-icons">image</span>}
												</div>
											}
										/>

										<div
											className="d-flex position-absolute"
											style={{ maxWidth: "296px" }}>
											<img src={file} style={{ maxHeight: "162px" }} />
										</div>
									</div>

									<Button
										onClick={() => {
											navigate(`/payment/ticket/${id}`);
											dispatch({
												type: "pilih_metode",
												payload: ["pilih-selesai", "upload-selesai", "bayar"],
											});
											dispatch({
												type: "invoice",
												payload: invNumber(),
											});
										}}
										className="btn-sewa-mobil font-14 w-100"
										disabled={disabled}>
										Upload
									</Button>
								</div>
							)}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default PaymentConfirm;
