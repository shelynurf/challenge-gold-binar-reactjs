import React, { useCallback, useEffect, useState } from "react";
import fi_users from "../../assets/images/fi_users.png";
import Button from "../../component/button";
import {
	Navigate,
	useLocation,
	useNavigate,
	useParams,
} from "react-router-dom";
import { fetchApi } from "../../config/services";
import {
	Accordion,
	AccordionBody,
	AccordionHeader,
	AccordionItem,
} from "reactstrap";
import PaymentNav from "../../component/paymentNav";
import { useDispatch, useSelector } from "react-redux";
import { Bank } from "../../component/bank";
import CustomLoader from "../../component/loader";
import { v4 as uuid } from "uuid";
import { Token } from "../../config/token";

const carSize = {
	small: "2 - 4 orang",
	medium: "4 - 6 orang",
	large: "6 - 8 orang",
};

const Payment = (props) => {
	const navigate = useNavigate();
	const [data, setData] = useState(null);
	const [loader, setLoader] = useState("idle");
	const { id } = useParams();
	const fetchingMobil = useCallback(
		(params = null) => {
			setLoader("fetching");
			fetchApi(
				// `https://api-car-rental.binaracademy.org/customer/car/${id}`,
				`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`,
				params
			)
				.then((result) => {
					// console.log("result :", result);
					setData(result.data);
					setLoader("resolve");
				})
				.catch((e) => {
					setLoader("reject");
				});
		},
		[id]
	);

	useEffect(() => {
		fetchingMobil();
	}, [fetchingMobil]);

	const formatNumber = (number) =>
		new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		})
			.format(number)
			.slice(0, -3);

	const [selectBank, setSelectBank] = useState("");
	const handleClick = (params) => {
		setSelectBank(params);
		setDisabled(false);
	};
	const found = Bank.find((item) => item.name === selectBank);

	const [disabled, setDisabled] = useState(true);
	const [expand, setExpand] = useState(false);

	const dispatch = useDispatch();
	useEffect(() => {
		dispatch({
			type: "pilih_metode",
			payload: ["pilih"],
		});
	}, [dispatch]);

	const { state } = useLocation();
	// console.log("state", state);
	const orderStart = useSelector((state) => state?.orderDate?.start);
	// const orderEnd = useSelector((state) => state?.orderDate?.end);
	// const countOrderDate = useSelector((state) => state?.orderDate?.countOrder);
	// const totalPrice = useSelector((state) => state?.orderDate?.totalPrice);

	// useEffect(() => {
	// 	dispatch({
	// 		type: "order-start",
	// 		payload: orderStart,
	// 	});
	// }, [dispatch]);

	const orderID = Math.floor(Math.random() * 10000000);

	if (!Token) return <Navigate to={"/"} />;

	// useEffect(() => {
	// 	dispatch(
	// 		{
	// 			type: "order-start",
	// 			payload: orderStart,
	// 		},
	// 		{
	// 			type: "order-end",
	// 			payload: orderEnd,
	// 		},
	// 		{
	// 			type: "count-order-date",
	// 			payload: countOrderDate,
	// 		},
	// 		{
	// 			type: "total-price",
	// 			payload: totalPrice,
	// 		}
	// 	);
	// }, [dispatch]);

	// useEffect(() => {
	// 	dispatch({
	// 		type: "order-start",
	// 		payload: orderStart,
	// 	});
	// }, [dispatch]);

	// useEffect(() => {
	// 	dispatch({
	// 		type: "order-end",
	// 		payload: orderEnd,
	// 	});
	// }, [dispatch]);
	// useEffect(() => {
	// 	dispatch({
	// 		type: "count-order-date",
	// 		payload: countOrderDate,
	// 	});
	// }, [dispatch]);
	// useEffect(() => {
	// 	dispatch({
	// 		type: "total-price",
	// 		payload: totalPrice,
	// 	});
	// }, [dispatch]);

	// const totalPrice = data?.price * countOrderDate;
	// console.log("orderDiff", countOrderDate);

	// console.log("type if format number", formatNumber(data?.price).slice(0, -3));

	return (
		<div className="container-fluid px-0">
			<div style={{ height: "166px", backgroundColor: "#f1f3ff" }}>
				<PaymentNav payNav="Pembayaran" />
				{/* <div className="mx-10 d-flex justify-content-between">
          <div className="d-flex gap-3 align-items-center">
            <button
              onClick={() => navigate(`/cari-mobil/${data.id}`)}
              style={{ border: "none", backgroundColor: "#f1f3ff" }}
            >
              <i className="fa-solid fa-arrow-left mt-1"></i>
            </button>
            <p className="font-14 mb-0">Pembayaran</p>
          </div>
          <div className="d-flex gap-3 align-items-center">
            <div>
              <i class="fa-solid fa-circle-1"></i>
              <p className="font-12 mb-0">Pilih metode</p>
            </div>
            <div>
              <hr className="line"></hr>
            </div>
            <div>
              <i class="fa-regular fa-circle-2"></i>
              <p className="font-12 mb-0">Bayar</p>
            </div>
            <div>
              <hr className="line"></hr>
            </div>
            <div>
              <i class="fa-regular fa-circle-3"></i>
              <p className="font-12 mb-0">Tiket</p>
            </div>
          </div>
        </div> */}
			</div>

			{/* {loader !== "resolve" && (
				<div
					style={{
						marginLeft: "10%",
						marginRight: "10%",
						paddingTop: "7rem",
						textAlign: "center",
					}}>
					<CustomLoader />
				</div>
			)} */}

			{/* {loader === "resolve" && ( */}
			<div className="container justify-content-center">
				<div className="px-2 d-flex justify-content-center">
					<div
						className="cari-mobil p-4 container"
						style={{ marginTop: "-5rem" }}>
						<div className="row gap-3">
							<div className="font-14">Detail Pesananmu</div>
							<div className="row font-14-thin">
								<div className="col">
									<p>Nama/Tipe Mobil</p>
									<p style={{ color: "#8A8A8A" }}>{data?.name}</p>
								</div>
								<div className="col">
									<p>Kategori</p>
									<p style={{ color: "#8A8A8A" }}>{carSize[data?.category]}</p>
								</div>
								<div className="col">
									<p>Tanggal Mulai Sewa</p>
									{/* belum pake API */}
									<p style={{ color: "#8A8A8A" }}>
										{/* {orderStart?.toLocaleDateString("in-ID", { */}
										{state?.orderStart.toLocaleDateString("in-ID", {
											day: "numeric",
											month: "long",
											year: "numeric",
										})}
									</p>
								</div>
								<div className="col">
									<p>Tanggal Akhir Sewa</p>
									{/* belum pake API */}
									<p style={{ color: "#8A8A8A" }}>
										{/* {orderEnd?.toLocaleDateString("in-ID", { */}
										{state?.orderEnd.toLocaleDateString("in-ID", {
											day: "numeric",
											month: "long",
											year: "numeric",
										})}
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div
					// className="mx-10"
					style={{ paddingTop: "6rem" }}>
					<div className="container">
						<div className="row gap-3 gap-md-0">
							<div className="col-md-7 ps-m-2 pe-m-3">
								<div className="card-car">
									<p className="font-14">Pilih Bank Transfer</p>
									<p className="font-14-thin">
										Kamu bisa membayar dengan transfer melalui ATM, Internet
										Banking atau Mobile Banking
									</p>

									{Bank.map((item, index) => {
										return (
											<div className="font-12">
												<div
													onClick={() => handleClick(item.name)}
													className="d-flex justify-content-between align-items-center"
													style={{ cursor: "pointer" }}>
													<div className="d-flex gap-3 align-items-center">
														<img
															src={item.bankImg}
															alt="bankImg"
															className="bank px-1 py-2"
														/>
														<span>{item.name} Transfer</span>
													</div>
													{item.name === selectBank && (
														<i
															className="fa-solid fa-check fa-2xl mb-3"
															style={{ color: "#5cb85f" }}></i>
													)}
												</div>

												<hr style={{ color: "#EEEEEE" }}></hr>
											</div>
										);
									})}
								</div>
							</div>

							<div className="col-md-5 pe-m-2 ps-m-3">
								<div className="card-car">
									<div className="mb-4">
										<p className="font-14">{data?.name}</p>
										<div className="d-flex align-items-start gap-2">
											<img src={fi_users} alt="users" />
											<p className="font-10-thin">{carSize[data?.category]}</p>
										</div>
									</div>

									{/* <Accordion open={open} toggle={toggle}>
									<AccordionItem style={{ border: "none" }}>
										<AccordionHeader targetId="total">
											<div className="d-flex justify-content-between">
												<div className="d-flex gap-4">
													<p className="font-14-thin">Total</p>
													
												</div>
												<p className="font-14">{formatNumber(totalPrice)}</p>
											</div>
										</AccordionHeader>
										<AccordionBody accordionId="total">
											<div>
												<p className="font-14">Harga</p>
												<div className="font-14-thin d-flex justify-content-between">
													<ul style={{ listStyle: "disc" }}>
														<li>
															Sewa Mobil {formatNumber(data?.price)} x{" "}
															{countOrderDate} Hari
														</li>
													</ul>
													<p>{formatNumber(totalPrice)}</p>
												</div>
											</div>
											<div>
												<p className="font-14">Biaya Lainnya</p>
												<div className="font-14-thin d-flex justify-content-between">
													<ul style={{ listStyle: "disc" }}>
														<li>Pajak</li>
													</ul>
													<p style={{ color: "#5CB85F" }}>Termasuk</p>
												</div>
												<div className="font-14-thin d-flex justify-content-between">
													<ul style={{ listStyle: "disc" }}>
														<li>Biaya makan sopir</li>
													</ul>
													<p style={{ color: "#5CB85F" }}>Termasuk</p>
												</div>
											</div>
											<div>
												<p className="font-14">Belum Termasuk</p>
												<div className="font-14-thin">
													<ul
														style={{
															flexDirection: "column",
															listStyle: "disc",
															gap: "1rem",
															alignItems: "flex-start",
														}}>
														<li>Bensin</li>
														<li>Tol dan parkir</li>
													</ul>
												</div>
											</div>
											<hr className="my-4" style={{ color: "#D0D0D0" }}></hr>
											<div className="font-14 d-flex justify-content-between">
												<p>Total</p>
												<p>{formatNumber(totalPrice)}</p>
											</div>
										</AccordionBody>
									</AccordionItem>
								</Accordion> */}

									<div
										className="d-flex justify-content-between mb-4"
										style={{ cursor: "pointer" }}
										onClick={() => setExpand(!expand)}>
										<div className="d-flex gap-4 align-items-end">
											<span className="font-14-thin">Total</span>
											<i
												className={`fa-solid  ${
													expand ? "fa-chevron-up" : "fa-chevron-down"
												}`}></i>
										</div>
										<span className="font-14">
											{/* {formatNumber(totalPrice)} */}
											{formatNumber(state?.totalPrice)}
										</span>
									</div>

									{expand && (
										<div>
											<div>
												<p className="font-14">Harga</p>
												<div className="font-14-thin d-flex justify-content-between">
													<ul style={{ listStyle: "disc" }}>
														<li>
															Sewa Mobil {formatNumber(data?.price)} x {""}
															{/* {formatNumber(data?.price)} x {countOrderDate} */}
															{state?.countOrderDate}
															{""} Hari
														</li>
													</ul>
													{/* <p>{formatNumber(totalPrice)}</p> */}
													<p>{formatNumber(state?.totalPrice)}</p>
												</div>
											</div>
											<div>
												<p className="font-14">Biaya Lainnya</p>
												<div className="font-14-thin d-flex justify-content-between">
													<ul style={{ listStyle: "disc" }}>
														<li>Pajak</li>
													</ul>
													<p style={{ color: "#5CB85F" }}>Termasuk</p>
												</div>
												<div className="font-14-thin d-flex justify-content-between">
													<ul style={{ listStyle: "disc" }}>
														<li>Biaya makan sopir</li>
													</ul>
													<p style={{ color: "#5CB85F" }}>Termasuk</p>
												</div>
											</div>
											<div>
												<p className="font-14">Belum Termasuk</p>
												<div className="font-14-thin">
													<ul
														style={{
															flexDirection: "column",
															listStyle: "disc",
															gap: "1rem",
															alignItems: "flex-start",
														}}>
														<li>Bensin</li>
														<li>Tol dan parkir</li>
													</ul>
												</div>
											</div>
											<hr className="my-4" style={{ color: "#D0D0D0" }}></hr>
											<div className="font-14 d-flex justify-content-between">
												<p>Total</p>
												{/* <p>{formatNumber(totalPrice)}</p> */}
												<p>{formatNumber(state?.totalPrice)}</p>
											</div>
										</div>
									)}

									<div>
										<Button
											onClick={() => {
												navigate(
													`/payment/confirm/${id}`
													// , {
													// 	state: {
													// 		...state,
													// 	},
													// }
												);
												dispatch({
													type: "pilih_metode",
													payload: ["pilih-selesai", "upload"],
												});
												dispatch({
													type: "konfirmasi",
													payload: found,
												});
												dispatch({
													type: "order-id",
													payload: orderID,
												});
											}}
											class="btn-sewa-mobil font-14 w-100"
											disabled={disabled}>
											Bayar
										</Button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{/* )} */}
		</div>
	);
};
export default Payment;
