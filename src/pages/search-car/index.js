import React, { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Button from "../../component/button";
import Input from "../../component/input";
import CustomLoader from "../../component/loader";
import SelectBox from "../../component/selectBox";
import { fetchApi } from "../../config/services";
import { Pagination } from "@mui/material";

const kategoriData = [
	{
		value: "small",
		label: "2 - 4 orang",
	},
	{
		value: "medium",
		label: "4 - 6 orang",
	},
	{
		value: "large",
		label: "6 - 8 orang",
	},
];

const priceData = [
	{
		value: "400",
		label: "< Rp. 400.000",
	},
	{
		value: "400-600",
		label: "Rp. 400.000 - Rp. 600.000",
	},
	{
		value: "600",
		label: "> Rp. 600.000",
	},
];

const statusData = [
	{
		value: true,
		label: "Disewa",
	},
	{
		value: false,
		label: "Tidak Disewa",
	},
];

const SearchCar = (props) => {
	const [value, setValue] = useState({
		carName: "",
		kategori: "",
		harga: "",
		status: "",
	});

	const [data, setData] = useState([]);
	const [page, setPage] = useState(1);
	const [loader, setLoader] = useState("resolve");
	const [backdrop, setBackdrop] = useState(false);

	const handleChange = (e) => {
		const { name, value } = e.target;
		setValue((prev) => ({
			...prev,
			[name]: value,
		}));
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		fetchingMobil({
			name: value.carName,
			category: value.kategori,
			isRented: value.status,
			Price: value.harga,
		});
		setBackdrop(false);
	};

	const [pagination, setPagination] = useState(false);

	document.addEventListener("click", (e) => {
		if (e.target.id === "back-drop") {
			setBackdrop(false);
		}
	});

	const [pagecount, setPagecount] = useState(0);

	const fetchingMobil = useCallback(
		(params = null) => {
			setLoader("fetching");
			// fetchApi(
			// 	"https://bootcamp-rent-cars.herokuapp.com/customer/v2/car?page=1&pageSize=100",
			// 	params
			// )
			fetchApi(
				`https://bootcamp-rent-cars.herokuapp.com/customer/v2/car?page=${page}`,
				params
			)
				.then((result) => {
					setPagecount(result.data.pageCount);
					setData(result.data.cars);
					setLoader("resolve");
					setPagination(true);
				})
				.catch((e) => {
					setLoader("reject");
				});
		},
		[page]
	);

	const handleChangePage = (page) => {
		setPage(page);
	};

	useEffect(() => {
		fetchingMobil();
	}, [page]);

	const formatNumber = (number) =>
		new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		})
			.format(number)
			.slice(0, -3);

	const navigate = useNavigate();

	return (
		<div className="container">
			{backdrop && <div id="back-drop" className="backdrop"></div>}
			<div className="d-flex justify-content-center">
				<div className="cari-mobil">
					<form onSubmit={handleSubmit}>
						<div className="d-flex gap-4 p-4 justify-content-center">
							<Input
								onFocus={() => {
									setBackdrop(true);
								}}
								onChange={handleChange}
								name="carName"
								label={"Nama Mobil"}
								placeholder="Ketik nama/tipe mobil"
							/>
							<SelectBox
								onFocus={() => {
									setBackdrop(true);
								}}
								onChange={handleChange}
								name="kategori"
								label={"Kategori"}
								data={kategoriData}
								placeholder="Masukan Kapasitas Mobil"
							/>
							<SelectBox
								onFocus={() => {
									setBackdrop(true);
								}}
								onChange={handleChange}
								name="harga"
								label={"Harga"}
								data={priceData}
								placeholder="Masukan Harga Sewa per Hari"
							/>
							<SelectBox
								onFocus={() => {
									setBackdrop(true);
								}}
								onChange={handleChange}
								name="status"
								label={"Status"}
								data={statusData}
								placeholder="Masukan Status Mobil"
							/>
							<div className="d-flex align-items-end">
								<Button className="btn-sewa-mobil">Cari Mobil</Button>
							</div>
							{/* <div className="d-flex align-items-end">
							<Button className="btn-tampil-semua">Tampilkan Semua</Button>
						</div> */}
						</div>
					</form>
				</div>
			</div>

			{/* {loader !== "resolve" && <div style={{marginLeft:"10%", marginRight:"10%", paddingTop:"7rem", textAlign:"center"}}>
                <Spinner className="m-5" color="primary">Loading</Spinner>
            </div>} */}

			{loader !== "resolve" && (
				<div
					style={{
						marginLeft: "10%",
						marginRight: "10%",
						paddingTop: "7rem",
						textAlign: "center",
					}}>
					<CustomLoader />
				</div>
			)}

			{loader === "resolve" && (
				<div
					className="row"
					style={{
						// marginLeft: "9.4%",
						// marginRight: "9.4%",
						paddingTop: "5rem",
					}}>
					{data?.map((item, index) => {
						return (
							<div className="col-sm-4 pb-4">
								<div className="card-car">
									<div className="image">
										<img
											className="max-size-image"
											src={item.image}
											alt="pict-car"
										/>
									</div>
									<div className="content">
										<p className="font-14-thin">{item.name}</p>
										<p className="font-16">{formatNumber(item.price)} / hari</p>
										<p className="font-14">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit,
											sed do eiusmod tempor incididunt ut labore et dolore magna
											aliqua.{" "}
										</p>
									</div>
									<div>
										<Button
											onClick={() => navigate(`/cari-mobil/${item.id}`)}
											className="btn-sewa-mobil font-14"
											style={{ width: "100%", height: "48px" }}>
											Pilih Mobil
										</Button>
									</div>
								</div>
							</div>
						);
					})}
				</div>
			)}

			{pagination && (
				<div className="d-flex justify-content-center">
					<Pagination
						onChange={(e) => handleChangePage(e.target.textContent)}
						count={pagecount}
						color="primary"
						variant="outlined"></Pagination>
				</div>
			)}
		</div>
	);
};

export default SearchCar;
