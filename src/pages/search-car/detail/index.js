import React, { useCallback, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Button from "../../../component/button";
import Input from "../../../component/input";
import SelectBox from "../../../component/selectBox";
import { fetchApi } from "../../../config/services";
import fi_users from "../../../assets/images/fi_users.png";
import CustomLoader from "../../../component/loader";
import Calendar from "../../../component/calendar";
import { useDispatch, useSelector } from "react-redux";
import { Token } from "../../../config/token";

const carSize = {
	small: "2 - 4 orang",
	medium: "4 - 6 orang",
	large: "6 - 8 orang",
};

const DetailCar = (props) => {
	const [data, setData] = useState(null);
	const [loader, setLoader] = useState("idle");
	const { id } = useParams();
	const fetchingMobil = useCallback(
		(params = null) => {
			setLoader("fetching");
			fetchApi(
				// `https://api-car-rental.binaracademy.org/customer/car/${id}`,
				`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`,
				params
			)
				.then((result) => {
					// console.log("result :", result);
					setData(result.data);
					setLoader("resolve");
				})
				.catch((e) => {
					setLoader("reject");
				});
		},
		[id]
	);

	useEffect(() => {
		fetchingMobil();
	}, [fetchingMobil]);

	const navigate = useNavigate();

	const formatNumber = (number) =>
		new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		})
			.format(number)
			.slice(0, -3);
	// console.log(loader);

	const dispatch = useDispatch();

	const orderStart = useSelector((state) => state?.orderDate?.start);
	const orderEnd = useSelector((state) => state?.orderDate?.end);
	const countOrderDate = useSelector((state) => state?.orderDate?.countOrder);
	const totalPrice = useSelector((state) => state?.orderDate?.totalPrice);

	const [calendar, setCalendar] = useState(false);

	document.addEventListener("click", (e) => {
		// console.log("target", e.target);
		// if (e.target.id === "back-drop") {
		// 	setBackdrop(false);
		// }
	});

	// useEffect(() => {
	// 	dispatch({
	// 		type: "order-start",
	// 		payload: orderStart,
	// 	});
	// 	dispatch({
	// 		type: "order-end",
	// 		payload: orderEnd,
	// 	});
	// 	dispatch({
	// 		type: "count-order-date",
	// 		payload: countOrderDate,
	// 	});
	// 	dispatch({
	// 		type: "total-price",
	// 		payload: totalPrice,
	// 	});
	// });
	// console.log("data", data);
	// const countOrderDate =
	// 	(orderDate[0].endDate.getTime() - orderDate[0].startDate.getTime()) /
	// 		(1000 * 60 * 60 * 24) +
	// 	1;
	// console.log("start date", order[endDate]);

	// const totalPrice = data.price * countOrderDate;

	return (
		<div className="container-fluid px-0">
			<div
				className="d-flex justify-content-center"
				style={{ height: "182px", backgroundColor: "#F1F3FF" }}>
				<div className="cari-mobil" style={{ marginTop: "7rem" }}>
					<p className="font-16 px-4 pt-4">Pencarianmu</p>
					<div className="d-flex gap-4 p-4 pt-0 justify-content-center">
						<Input
							disabled={true}
							name="carName"
							label={"Nama Mobil"}
							placeholder="Ketik nama/tipe mobil"
						/>
						<SelectBox
							disabled={true}
							name="kategori"
							label={"Kategori"}
							placeholder="Masukan Kapasitas Mobil"
						/>
						<SelectBox
							disabled={true}
							name="harga"
							label={"Harga"}
							placeholder="Masukan Harga Sewa per Hari"
						/>
						<SelectBox
							disabled={true}
							name="status"
							label={"Status"}
							placeholder="Masukan Status Mobil"
						/>
					</div>
				</div>
			</div>
			{/* {loader !== "resolve" && <div style={{marginLeft:"10%", marginRight:"10%", paddingTop:"7rem", textAlign:"center"}}>
                <Spinner className="m-5" color="primary">Loading</Spinner>
            </div>} */}

			{loader !== "resolve" && (
				<div
					style={{
						marginLeft: "10%",
						marginRight: "10%",
						paddingTop: "7rem",
						textAlign: "center",
					}}>
					<CustomLoader />
				</div>
			)}

			{loader === "resolve" && (
				<div>
					<div
						className="row"
						style={{
							marginLeft: "10%",
							marginRight: "10%",
							paddingTop: "7rem",
							gap: "24px",
						}}>
						<div className="col card-car p-5 d-grid">
							<p className="font-14">Tentang Paket</p>
							<div>
								<p className="font-14">Include</p>
								<ul className="detail-car-list">
									<li>
										Apa saja yang termasuk dalam paket misal durasi max 12 jam
									</li>
									<li>Sudah termasuk bensin selama 12 jam</li>
									<li>Sudah termasuk Tiket Wisata</li>
									<li>Sudah termasuk pajak</li>
								</ul>
							</div>
							<div>
								<p className="font-14">Exclude</p>
								<ul className="detail-car-list">
									<li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
									<li>
										Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
										20.000/jam
									</li>
									<li>Tidak termasuk akomodasi penginapan</li>
								</ul>
							</div>
							<div>
								<p className="font-14">Refund, Reschedule, Overtime</p>
								<ul className="detail-car-list">
									<li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
									<li>
										Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
										20.000/jam
									</li>
									<li>Tidak termasuk akomodasi penginapan</li>
									<li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
									<li>
										Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
										20.000/jam
									</li>
									<li>Tidak termasuk akomodasi penginapan</li>
									<li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
									<li>
										Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
										20.000/jam
									</li>
									<li>Tidak termasuk akomodasi penginapan</li>
								</ul>
							</div>
						</div>
						<div
							className="col-md-4 card-car"
							style={{ height: "fit-content" }}>
							<div className="px-4">
								<img
									style={{ maxWidth: "100%" }}
									src={data?.image}
									alt="pict-car"
								/>
							</div>
							<div className="row px-4 gap-2 mt-5">
								<div>
									<p className="font-14">{data?.name}</p>
									<div className="d-flex align-items-start gap-1">
										<img src={fi_users} alt="icon_users" />
										<p className="font-10">{carSize[data?.category]}</p>
									</div>
								</div>
								<div>
									<div className="font-12-thin">
										Tentukan lama sewa mobil (max. 7 hari)
									</div>
									{/* <Input
										onFocus={() => {
											setCalendar(true);
										}}
										placeholder="Pilih tanggal mulai dan tanggal akhir sewa"
									/>
									{calendar && <Calendar />} */}
									<Calendar />
								</div>
								<div className="d-flex justify-content-between py-4">
									<p className="font-14">Total</p>
									<p className="font-14">{formatNumber(totalPrice)}</p>
								</div>
							</div>

							<div className="px-4">
								<Button
									// onClick={() => {
									// 	navigate(`/payment/${id}`);
									// 	dispatch({
									// 		type: "order-start",
									// 		payload: orderStart,
									// 	});
									// 	dispatch({
									// 		type: "order-end",
									// 		payload: orderEnd,
									// 	});
									// 	dispatch({
									// 		type: "count-order-date",
									// 		payload: countOrderDate,
									// 	});
									// 	dispatch({
									// 		type: "total-price",
									// 		payload: totalPrice,
									// 	});
									// }}
									onClick={() =>
										Token
											? navigate(`/payment/${id}`, {
													state: {
														orderStart,
														orderEnd,
														countOrderDate,
														totalPrice,
													},
											  })
											: navigate("/login")
									}
									className="font-14 btn-sewa-mobil w-100">
									Lanjutkan Pembayaran
								</Button>
							</div>
						</div>
					</div>
				</div>
			)}
		</div>
	);
};

export default DetailCar;
