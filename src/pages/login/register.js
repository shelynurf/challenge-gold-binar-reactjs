import React from "react";
import { Form, Input } from "reactstrap";
import Button from "../../component/button";
import { Navigate, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useState } from "react";
import { Services } from "../../config/services";
import { Token } from "../../config/token";

const Register = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const [state, setstate] = useState({
		name: "",
		email: "",
		password: "",
	});

	const handleChange = (e) => {
		const { name, value } = e.target;
		setstate((prev) => ({
			...prev,
			[name]: value,
		}));
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		Services()
			.post("https://bootcamp-rent-cars.herokuapp.com/customer/auth/register", {
				...state,
				role: "Admin",
			})
			.then((response) => {
				navigate("/login");
			})
			.catch((err) => console.log(err.message));
	};

	if (Token) return <Navigate to={"/"} />;
	return (
		<div
			className="d-grid justify-content-center gap-5"
			style={{ paddingTop: "8rem", paddingBottom: "5rem" }}>
			<div
				onClick={() => navigate("/")}
				className="title"
				style={{
					position: "inherit",
					background: "#CFD4ED",
					cursor: "pointer",
				}}></div>
			<p className="font-24 mb-0">Sign Up</p>
			<Form onSubmit={handleSubmit}>
				<div
					className="container-fluid d-grid gap-3"
					style={{ width: "25rem" }}>
					<div className="d-grid gap-3">
						<label className="font-14-thin d-flex gap-1">
							<span>Name</span>
							<span style={{ color: "red" }}>*</span>
						</label>
						<Input
							onChange={handleChange}
							name="name"
							type="text"
							placeholder="Nama Lengkap"></Input>
					</div>
					<div className="d-grid gap-3">
						<label className="font-14-thin d-flex gap-1">
							<span>Email</span>
							<span style={{ color: "red" }}>*</span>
						</label>
						<Input
							onChange={handleChange}
							name="email"
							type="text"
							placeholder="Contoh: johndee@gmail.com"></Input>
					</div>
					<div
						className="d-grid gap-3"
						//  md={8} className="pb-4"
					>
						<label className="font-14-thin d-flex gap-1">
							<span>Create Password</span>
							<span style={{ color: "red" }}>*</span>
						</label>
						<Input
							onChange={handleChange}
							name="password"
							type="password"
							placeholder="6+ karakter"></Input>
					</div>
					<div className="pt-2">
						<Button className="btn-signin font-14 w-100">Sign Up</Button>
						{/* <Button className="w-100 btn-signin">Sign In</Button> */}
					</div>
				</div>
			</Form>
			<div className="d-flex justify-content-center gap-2">
				<span className="font-14-thin">Already have an account?</span>
				<a
					onClick={() => navigate("/login")}
					className="font-14"
					style={{ color: "#0D28A6", cursor: "pointer" }}>
					Sign In here
				</a>
			</div>
		</div>
	);
};

export default Register;
