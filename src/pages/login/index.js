import React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { Form, Input, Row } from "reactstrap";
import Button from "../../component/button";
import { Navigate, useNavigate } from "react-router-dom";
import { Services } from "../../config/services";
import { Token } from "../../config/token";
import { postApi } from "../../config/services";

const Login = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const [state, setstate] = useState({
		email: "",
		password: "",
	});

	const handleChange = (e) => {
		const { name, value } = e.target;
		setstate((prev) => ({
			...prev,
			[name]: value,
		}));
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		Services()
			.post("https://bootcamp-rent-cars.herokuapp.com/customer/auth/login", {
				...state,
			})
			.then((response) => {
				const { data } = response;
				localStorage.setItem("ACCESS_TOKEN", data.access_token);

				window.location.replace("/");
			})
			.catch((err) => console.log(err.message));

		// postApi("https://api-car-rental.binaracademy.org/customer/auth/login", {
		// 	...state,
		// })
		// 	// fetchApi("https://bootcamp-rent-cars.herokuapp.com/customer/v2/car", params)
		// 	.then((response) => {
		// 		// console.log("result : ", result);
		// 		const { data } = response;
		// 		localStorage.setItem("ACCESS_TOKEN", data.access_token);
		// 		navigate("/");
		// 	})
		// 	.catch((err) => err.message);
		// dispatch({
		// 	type: "LOGGED IN",
		// 	payload: "is_LoggedIn",
		// })
	};

	// if (Token) return <Navigate to={"/"} />;
	return (
		<div
			className="d-grid justify-content-center gap-5"
			style={{ paddingTop: "8rem", paddingBottom: "5rem" }}>
			<div
				onClick={() => navigate("/")}
				className="title"
				style={{
					position: "inherit",
					background: "#CFD4ED",
					cursor: "pointer",
				}}></div>
			<p className="font-24 mb-0">Welcome Back!</p>
			<Form onSubmit={handleSubmit}>
				<div
					className="container-fluid d-grid gap-3"
					style={{ width: "25rem" }}>
					<div className="d-grid gap-3">
						<label className="font-14-thin">Email</label>
						<Input
							onChange={handleChange}
							name="email"
							type="text"
							placeholder="Contoh: johndee@gmail.com"></Input>
					</div>
					<div
						className="d-grid gap-3"
						//  md={8} className="pb-4"
					>
						<label className="font-14-thin">Password</label>
						<Input
							onChange={handleChange}
							name="password"
							type="password"
							placeholder="6+ karakter"></Input>
					</div>
					<div className="pt-2">
						<Button className="btn-signin font-14 w-100">Sign In</Button>
						{/* <Button className="w-100 btn-signin">Sign In</Button> */}
					</div>
				</div>
			</Form>
			<div className="d-flex justify-content-center gap-2">
				<span className="font-14-thin">Don’t have an account?</span>
				<a
					onClick={() => navigate("/register")}
					className="font-14"
					style={{ color: "#0D28A6", cursor: "pointer" }}>
					Sign Up for free
				</a>
			</div>
		</div>
	);
};

export default Login;
