import React, { useState } from "react";
import heroService from "../../assets/images/happy girl.png";
import checklist from "../../assets/images/checklist.png";
import icon_complete from "../../assets/images/icon_complete.png";
import icon_price from "../../assets/images/icon_price.png";
import icon_24hrs from "../../assets/images/icon_24hrs.png";
import icon_professional from "../../assets/images/icon_professional.png";
import rate from "../../assets/images/Rate.png";
import profile1 from "../../assets/images/img_photo.png";
import profile2 from "../../assets/images/img_photo (1).png";
import "../../assets/css/index.css";
import Button from "../../component/button";
import { useNavigate } from "react-router-dom";
import {
	Accordion,
	AccordionBody,
	AccordionHeader,
	AccordionItem,
} from "reactstrap";
import { Swiper, SwiperSlide, useSwiper } from "swiper/react";
import { Navigation, Pagination } from "swiper";
// import "swiper/css";
// import "swiper/css/navigation";
// import "swiper/css/pagination";
// import "swiper/css/scrollbar";
import "swiper/swiper-bundle.css";

import { SwiperNavButtons } from "../../component/SwipperNavButtons";

const Home = (props) => {
	const navigate = useNavigate();
	const [open, setOpen] = useState("0");
	const toggle = (id) => {
		if (open === id) {
			setOpen();
		} else {
			setOpen(id);
		}

		// console.log("open :", open);
		// console.log("setopen :", setOpen);
	};
	const swiper = useSwiper();

	return (
		<div className="home">
			{/* <i
				class="fa-solid fa-arrow-up fa-2xl"
				style={{
					color: "#5CB85F",
					position: "sticky",
					left: "200vh",
					top: "90vh",
				}}></i> */}
			<section id="our-service" className="container m-sec">
				{/* <div className="row px-8"> */}
				<div className="row gap-4">
					<div className="col-md col-lg col-sm-12">
						<div>
							<img src={heroService} alt="alt-pict" className="img-fluid" />
						</div>
					</div>
					<div className="col">
						<div>
							<h2 className="font-24 mb-5">
								Best Car Rental for any kind of trip in Purwokerto!
							</h2>
							<div className="font-14">
								<p className="mb-4">
									Sewa mobil di Purwokerto bersama Binar Car Rental jaminan
									harga lebih murah dibandingkan yang lain, kondisi mobil baru,
									serta kualitas pelayanan terbaik untuk perjalanan wisata,
									bisnis, wedding, meeting, dll.
								</p>
								<ul className="row font-14 gap-3 ps-0">
									<li className="d-flex gap-2">
										<img
											src={checklist}
											alt="alt-pict"
											style={{ height: "fit-content" }}
										/>
										Sewa Mobil Dengan Supir di Bali 12 Jam
									</li>
									<li className="d-flex gap-2">
										<img
											src={checklist}
											alt="alt-pict"
											style={{ height: "fit-content" }}
										/>
										Sewa Mobil Lepas Kunci di Bali 24 Jam
									</li>
									<li className="d-flex gap-2">
										<img
											src={checklist}
											alt="alt-pict"
											style={{ height: "fit-content" }}
										/>
										Sewa Mobil Jangka Panjang Bulanan
									</li>
									<li className="d-flex gap-2">
										<img
											src={checklist}
											alt="alt-pict"
											style={{ height: "fit-content" }}
										/>
										Gratis Antar - Jemput Mobil di Bandara
									</li>
									<li className="d-flex gap-2">
										<img
											src={checklist}
											alt="alt-pict"
											style={{ height: "fit-content" }}
										/>
										Layanan Airport Transfer / Drop In Out
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			{/* <!-- end list best car -->

            <!-- Start Why us--> */}
			{/* <section id="why-us" className="px-8 m-sec"> */}
			<section id="why-us" className="container m-sec">
				<div className="d-grid text-center">
					<h2 className="font-24">Why Us?</h2>
					<p className="font-14 my-4">Mengapa harus pilih Binar Car Rental?</p>
				</div>
				<div className="row row-cols-1 row-cols-md-2 row-cols-lg-4">
					<div className="p-2">
						<div className="col card h-100 align-items-start gap-3 p-4">
							<div className="card-body">
								<img src={icon_complete} alt="alt-pict" />
								<h3 className="font-16">Mobil Lengkap</h3>
								<p className="font-14">
									Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
									terawat
								</p>
							</div>
						</div>
					</div>
					<div className="p-2">
						<div className="col card h-100 align-items-start gap-3 p-4">
							<div className="card-body">
								<img src={icon_price} alt="alt-pict" />
								<h3 className="font-16">Harga Murah</h3>
								<p className="font-14">
									Harga murah dan bersaing, bisa bandingkan harga kami dengan
									rental mobil lain
								</p>
							</div>
						</div>
					</div>
					<div className="p-2">
						<div className="col card h-100 align-items-start gap-3 p-4">
							<div className="card-body">
								<img src={icon_24hrs} alt="alt-pict" />
								<h3 className="font-16">Layanan 24 Jam</h3>
								<p className="font-14">
									Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
									tersedia di akhir minggu
								</p>
							</div>
						</div>
					</div>
					<div className="p-2">
						<div className="col card h-100 align-items-start gap-3 p-4">
							<div className="card-body">
								<img src={icon_professional} alt="alt-pict" />
								<h3 className="font-16">Sopir Profesional</h3>
								<p className="font-14">
									Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
									tepat waktu
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>

			{/* <div className="d-flex justify-content-center">
				<div className="p-2" style={{ maxWidth: "619px", minHeight: "270px" }}>
					<div
						className="col card border border-0 p-4"
						style={{ background: "#F1F3FF" }}>
						<div className="card-body">
							<div className="row gap-5 justify-content-center">
								<div className="col-lg col-sm-12 d-flex justify-content-center align-items-center">
									<img src={profile2} alt="profile" className="img-fluid" />
								</div>
								<div className="col col-sm-12 col-md-8">
									<div className="d-flex justify-content-center justify-content-lg-start">
										<img src={rate} alt="rating" />
									</div>
									<p className="font-14 mt-3">
										“Lorem ipsum dolor sit amet, consectetur adipiscing elit,
										sed do eiusmod lorem ipsum dolor sit amet, consectetur
										adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
										consectetur adipiscing elit, sed do eiusmod”
									</p>
									<p>John Dee 32, Bromo</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> */}
			<section id="testimonial" className="container m-sec">
				<div className="d-grid text-center">
					<h2 className="font-24">Testimonial</h2>
					<p className="font-14 my-4">
						Berbagai review positif dari para pelanggan kami
					</p>
				</div>
				<div>
					<Swiper
						slidesPerView={1}
						spaceBetween={10}
						loop={true}
						slidesPerGroup={1}
						breakpoints={{
							// 640: {
							// 	slidesPerView: 2,
							// 	spaceBetween: 20,
							// },
							// 768: {
							// 	slidesPerView: 4,
							// 	spaceBetween: 40,
							// },
							1024: {
								slidesPerView: 3,
								spaceBetween: 50,
							},
						}}
						className="mySwiper">
						<SwiperSlide>
							<div className="d-flex justify-content-center">
								<div
									className="p-2"
									style={{ maxWidth: "619px", minHeight: "270px" }}>
									<div
										className="col card border border-0 p-4"
										style={{ background: "#F1F3FF" }}>
										<div className="card-body">
											<div className="row gap-5 justify-content-center">
												<div className="col-lg col-sm-12 d-flex justify-content-center align-items-center">
													<img
														src={profile2}
														alt="profile"
														className="img-fluid"
													/>
												</div>
												<div className="col col-sm-12 col-md-8">
													<div className="d-flex justify-content-center justify-content-lg-start">
														<img src={rate} alt="rating" />
													</div>
													<p className="font-14 mt-3">
														“Lorem ipsum dolor sit amet, consectetur adipiscing
														elit, sed do eiusmod lorem ipsum dolor sit amet,
														consectetur adipiscing elit, sed do eiusmod lorem
														ipsum dolor sit amet, consectetur adipiscing elit,
														sed do eiusmod”
													</p>
													<p>John Dee 32, Bromo</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</SwiperSlide>
						<SwiperSlide>
							<div className="d-flex justify-content-center">
								<div
									className="p-2"
									style={{ maxWidth: "619px", minHeight: "270px" }}>
									<div
										className="col card border border-0 p-4"
										style={{ background: "#F1F3FF" }}>
										<div className="card-body">
											<div className="row gap-5 justify-content-center">
												<div className="col-lg col-sm-12 d-flex justify-content-center align-items-center">
													<img
														src={profile1}
														alt="profile"
														className="img-fluid"
													/>
												</div>
												<div className="col col-sm-12 col-md-8">
													<div className="d-flex justify-content-center justify-content-lg-start">
														<img src={rate} alt="rating" />
													</div>
													<p className="font-14 mt-3">
														“Lorem ipsum dolor sit amet, consectetur adipiscing
														elit, sed do eiusmod lorem ipsum dolor sit amet,
														consectetur adipiscing elit, sed do eiusmod lorem
														ipsum dolor sit amet, consectetur adipiscing elit,
														sed do eiusmod”
													</p>
													<p>John Dee 32, Bromo</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</SwiperSlide>
						<SwiperSlide>
							<div className="d-flex justify-content-center">
								<div
									className="p-2"
									style={{ maxWidth: "619px", minHeight: "270px" }}>
									<div
										className="col card border border-0 p-4"
										style={{ background: "#F1F3FF" }}>
										<div className="card-body">
											<div className="row gap-5 justify-content-center">
												<div className="col-lg col-sm-12 d-flex justify-content-center align-items-center">
													<img
														src={profile2}
														alt="profile"
														className="img-fluid"
													/>
												</div>
												<div className="col col-sm-12 col-md-8">
													<div className="d-flex justify-content-center justify-content-lg-start">
														<img src={rate} alt="rating" />
													</div>
													<p className="font-14 mt-3">
														“Lorem ipsum dolor sit amet, consectetur adipiscing
														elit, sed do eiusmod lorem ipsum dolor sit amet,
														consectetur adipiscing elit, sed do eiusmod lorem
														ipsum dolor sit amet, consectetur adipiscing elit,
														sed do eiusmod”
													</p>
													<p>John Dee 32, Bromo</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</SwiperSlide>
						<SwiperSlide>
							<div className="d-flex justify-content-center">
								<div
									className="p-2"
									style={{ maxWidth: "619px", minHeight: "270px" }}>
									<div
										className="col card border border-0 p-4"
										style={{ background: "#F1F3FF" }}>
										<div className="card-body">
											<div className="row gap-5 justify-content-center">
												<div className="col-lg col-sm-12 d-flex justify-content-center align-items-center">
													<img
														src={profile1}
														alt="profile"
														className="img-fluid"
													/>
												</div>
												<div className="col col-sm-12 col-md-8">
													<div className="d-flex justify-content-center justify-content-lg-start">
														<img src={rate} alt="rating" />
													</div>
													<p className="font-14 mt-3">
														“Lorem ipsum dolor sit amet, consectetur adipiscing
														elit, sed do eiusmod lorem ipsum dolor sit amet,
														consectetur adipiscing elit, sed do eiusmod lorem
														ipsum dolor sit amet, consectetur adipiscing elit,
														sed do eiusmod”
													</p>
													<p>John Dee 32, Bromo</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</SwiperSlide>
						<SwiperNavButtons />
					</Swiper>
				</div>
			</section>

			<section id="faq" className="container m-sec">
				{/* <div className="row px-8"> */}
				<div className="row">
					<div className="col-sm">
						<h2 className="font-24">Frequently Asked Question</h2>
						<p className="font-14">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit
						</p>
					</div>
					<div className="col-sm col-md">
						<Accordion
							open={open}
							toggle={toggle}
							className="d-grid gap-3 accord"
							flush>
							<AccordionItem className="accord-items">
								<AccordionHeader targetId="1">
									Apa saja syarat yang dibutuhkan?
								</AccordionHeader>
								<AccordionBody accordionId="1">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
									do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									Ut enim ad minim veniam, quis nostrud exercitation ullamco
									laboris nisi ut aliquip ex ea commodo consequat. Duis aute
									irure dolor in reprehenderit in voluptate velit esse cillum
									dolore eu fugiat nulla pariatur. Excepteur sint occaecat
									cupidatat non proident, sunt in culpa qui officia deserunt
									mollit anim id est laborum.
								</AccordionBody>
							</AccordionItem>
							<AccordionItem className="accord-items">
								<AccordionHeader targetId="2">
									Berapa hari minimal sewa mobil lepas kunci?
								</AccordionHeader>
								<AccordionBody accordionId="2">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
									do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									Ut enim ad minim veniam, quis nostrud exercitation ullamco
									laboris nisi ut aliquip ex ea commodo consequat. Duis aute
									irure dolor in reprehenderit in voluptate velit esse cillum
									dolore eu fugiat nulla pariatur. Excepteur sint occaecat
									cupidatat non proident, sunt in culpa qui officia deserunt
									mollit anim id est laborum.
								</AccordionBody>
							</AccordionItem>
							<AccordionItem className="accord-items">
								<AccordionHeader targetId="3">
									Berapa hari sebelumnya sabaiknya booking sewa mobil?
								</AccordionHeader>
								<AccordionBody accordionId="3">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
									do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									Ut enim ad minim veniam, quis nostrud exercitation ullamco
									laboris nisi ut aliquip ex ea commodo consequat. Duis aute
									irure dolor in reprehenderit in voluptate velit esse cillum
									dolore eu fugiat nulla pariatur. Excepteur sint occaecat
									cupidatat non proident, sunt in culpa qui officia deserunt
									mollit anim id est laborum.
								</AccordionBody>
							</AccordionItem>
							<AccordionItem className="accord-items">
								<AccordionHeader targetId="4">
									Apakah Ada biaya antar-jemput?
								</AccordionHeader>
								<AccordionBody accordionId="4">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
									do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									Ut enim ad minim veniam, quis nostrud exercitation ullamco
									laboris nisi ut aliquip ex ea commodo consequat. Duis aute
									irure dolor in reprehenderit in voluptate velit esse cillum
									dolore eu fugiat nulla pariatur. Excepteur sint occaecat
									cupidatat non proident, sunt in culpa qui officia deserunt
									mollit anim id est laborum.
								</AccordionBody>
							</AccordionItem>
							<AccordionItem className="accord-items">
								<AccordionHeader targetId="5">
									Bagaimana jika terjadi kecelakaan
								</AccordionHeader>
								<AccordionBody accordionId="5">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
									do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									Ut enim ad minim veniam, quis nostrud exercitation ullamco
									laboris nisi ut aliquip ex ea commodo consequat. Duis aute
									irure dolor in reprehenderit in voluptate velit esse cillum
									dolore eu fugiat nulla pariatur. Excepteur sint occaecat
									cupidatat non proident, sunt in culpa qui officia deserunt
									mollit anim id est laborum.
								</AccordionBody>
							</AccordionItem>
						</Accordion>
					</div>
				</div>
			</section>
		</div>
	);
};

export default Home;
