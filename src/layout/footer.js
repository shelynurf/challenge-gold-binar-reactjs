import React from "react";
import facebook from "../assets/images/icon_facebook.png";
import instagram from "../assets/images/icon_instagram.png";
import twitter from "../assets/images/icon_twitter.png";
import mail from "../assets/images/icon_mail.png";
import twitch from "../assets/images/icon_twitch.png";
import { useNavigate } from "react-router-dom";

const Footer = () => {
	const navigate = useNavigate();
	return (
		<div className="container m-sec">
			<div className="row">
				<div className="col">
					<div className="row gap-3">
						<div className="col-lg col-md col-sm-12 font-14">
							<p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
							<p>binarcarrental@gmail.com</p>
							<p>081-233-334-808</p>
						</div>
						<div className="col font-14-thin">
							<ul className="row gap-3 ps-0">
								<li onClick={() => navigate("/#our-service")}>
									<a href="#our-service">Our services</a>
								</li>
								<li onClick={() => navigate("/#why-us")}>
									<a href="#why-us">Why Us</a>
								</li>
								<li onClick={() => navigate("/#testimonial")}>
									<a href="#testimonial">Testimonial</a>
								</li>
								<li onClick={() => navigate("/#faq")}>
									<a href="#faq">FAQ</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div className="col">
					<div className="row gap-3">
						<div className="col-lg col-md col-sm-12 font-14">
							<p>Connect with us</p>
							<div className="d-flex gap-2">
								<img src={facebook} alt="connect-us" />
								<img src={instagram} alt="connect-us" />
								<img src={twitter} alt="connect-us" />
								<img src={mail} alt="connect-us" />
								<img src={twitch} alt="connect-us" />
							</div>
						</div>
						<div className="col font-14">
							<p>Copyright Binar 2022</p>
							<div
								className="title"
								onClick={() => navigate("/")}
								style={{ cursor: "pointer" }}></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Footer;
