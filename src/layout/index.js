import React from "react";
import PublicRoutes from "../config/router";
import Footer from "./footer";
import Header from "./header";
import PublicAuthorized from "../config/router-public";
import { useLocation } from "react-router-dom";

const LoginRoutes = (props) => {
	return <PublicAuthorized />;
};

const InlineRoutes = (props) => {
	return (
		<div>
			<Header />
			<PublicRoutes />
			<Footer />
		</div>
	);
};

const Layout = (props) => {
	const { pathname } = useLocation();
	return (
		<div>
			{pathname !== "/login" && pathname !== "/register" ? (
				<InlineRoutes {...props} />
			) : (
				<LoginRoutes {...props} />
			)}
		</div>
	);
};

export default Layout;
