import React from "react";
import { useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import ImageCar from "../assets/images/img_car.png";
import Button from "../component/button";
import { Token } from "../config/token";

const Header = (props) => {
	const navigate = useNavigate();
	const location = useLocation();
	// console.log(location);

	const [open, setOpen] = useState(false);
	const openSideBar = () => {
		setOpen(!open);
		setBackdrop(!backdrop);
	};

	const closeSidebar = () => {
		setOpen(false);
		setBackdrop(false);
	};

	const [backdrop, setBackdrop] = useState(false);
	const params = useLocation();
	// console.log("params :", params.pathname.split('/').filter(i => i !== "")[1])
	// console.log("params :", params.pathname.split('/').filter(i => (i === "/" || i === "/cari-mobil")))
	// console.log(
	// 	"params :",
	// 	params.pathname.split("/").filter((i) => i !== "")[1]
	// );
	return (
		<header className="container-fluid bg-gray">
			{/* <div className="d-flex justify-content-around"> */}
			<div className="container">
				<div className="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-between align-items-center">
					<div
						className="title"
						onClick={() => navigate("/")}
						style={{ cursor: "pointer" }}></div>
					<nav className="font-14 p-4 d-none d-md-flex">
						<ul className="font-14-thin">
							<li onClick={() => navigate("/#our-service")}>
								<a href="#our-service">Our Service</a>
							</li>
							<li onClick={() => navigate("/#why-us")}>
								<a href="#why-us">Why Us</a>
							</li>
							<li onClick={() => navigate("/#testimonial")}>
								<a href="#testimonial">Testimonial</a>
							</li>
							<li onClick={() => navigate("/#faq")}>
								<a href="#faq">FAQ</a>
							</li>
							<li>
								{Token ? (
									<Button
										onClick={() => {
											localStorage.removeItem("ACCESS_TOKEN");
											window.location.reload();
										}}
										className="font-14 btn-sewa-mobil bg-danger">
										Logout
									</Button>
								) : (
									<Button
										onClick={() => navigate("/register")}
										className="font-14 btn-sewa-mobil">
										Register
									</Button>
								)}
							</li>
						</ul>
					</nav>
					<Button id="click-btn" className="btn d-md-none d-lg-none d-xl-none ">
						<i className="fa fa-bars" onClick={() => openSideBar()}></i>
					</Button>
				</div>
				{backdrop && <div id="back-drop" className="backdrop"></div>}
				{open && (
					<aside className="sidebar px-3 py-4">
						<div className="d-grid gap-4">
							<div className="d-flex justify-content-between">
								<span className="font-14">BCR</span>
								<i class="fa-solid fa-xmark" onClick={() => openSideBar()}></i>
							</div>
							<div>
								<ul className="d-grid ps-0 gap-4 font-14-thin">
									<li>
										<a href="#our-service" onClick={() => closeSidebar()}>
											Our Service
										</a>
									</li>
									<li>
										<a href="#why-us" onClick={() => closeSidebar()}>
											Why Us
										</a>
									</li>
									<li>
										<a href="#testimonial" onClick={() => closeSidebar()}>
											Testimonial
										</a>
									</li>
									<li>
										<a href="#faq" onClick={() => closeSidebar()}>
											FAQ
										</a>
									</li>
									<li>
										{Token ? (
											<Button
												onClick={() => localStorage.removeItem("ACCESS_TOKEN")}
												className="font-14 btn-sewa-mobil bg-danger">
												Logout
											</Button>
										) : (
											<Button
												onClick={() => navigate("/register")}
												className="font-14 btn-sewa-mobil">
												Register
											</Button>
										)}
									</li>
								</ul>
							</div>
						</div>
					</aside>
				)}
			</div>

			{/* button sementara */}
			{/* <div style={{ position: "absolute", textAlign: "center", top: "1rem" }}>
        <Button
          onClick={() => navigate("/payment")}
          className="font-14 btn-sewa-mobil"
          nama="Pembayaran"
        />
      </div> */}

			{!params.pathname.split("/").filter((i) => i !== "")[1] &&
				!params.pathname.split("/").filter((i) => i === "payment")[0] && (
					<div className="container">
						<div className="row mt-4 align-items-center">
							<div className="col">
								<h1 className="font-36">
									Sewa & Rental Mobil Terbaik di kawasan Purwokerto
								</h1>
								<p className="font-14">
									Selamat datang di Binar Car Rental. Kami menyediakan mobil
									kualitas terbaik dengan harga terjangkau. Selalu siap melayani
									kebutuhanmu untuk sewa mobil selama 24 jam.
								</p>
								{!params.pathname
									.split("/")
									.filter((i) => i === "cari-mobil")[0] && (
									<Button
										onClick={() => navigate("/cari-mobil")}
										className="font-14 btn-sewa-mobil">
										Mulai Sewa Mobil
									</Button>
								)}
							</div>

							<div className="col-lg col-md col-sm-12 d-flex justify-content-end">
								<img src={ImageCar} alt="alt-pict" className="img-fluid" />
							</div>
						</div>
					</div>
				)}
			{/* <sideBar open={open} close={setOpen} /> */}
		</header>
	);
};

export default Header;
