const initialState = {
	start: null,
	end: null,
	countOrder: 0,
	totalPrice: 0,
	orderID: null,
	invoice: null,
};

export const orderDate = (state = initialState, action) => {
	switch (action.type) {
		case "order-start":
			return {
				...state,
				start: action.payload,
			};
		case "order-end":
			return {
				...state,
				end: action.payload,
			};
		case "count-order-date":
			return {
				...state,
				countOrder: action.payload,
			};
		case "total-price":
			return {
				...state,
				totalPrice: action.payload,
			};
		case "order-id":
			return {
				...state,
				orderID: action.payload,
			};
		case "invoice":
			return {
				...state,
				invoice: action.payload,
			};
		default:
			return {
				...state,
			};
	}
};
