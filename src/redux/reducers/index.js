import { combineReducers } from "redux";
import AuthLogin from "./login-authentication";
import { stepper } from "./stepper";
import { orderDate } from "./orderDate";

const rootReducer = combineReducers({
	AuthLogin,
	stepper,
	orderDate,
});

export default rootReducer;
