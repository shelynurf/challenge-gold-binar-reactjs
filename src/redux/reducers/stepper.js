const initialState = {
	step: [],
	konfirmasi: null,
};

export const stepper = (state = initialState, action) => {
	switch (action.type) {
		case "konfirmasi":
			return {
				...state,
				konfirmasi: action.payload,
			};
		case "pilih_metode":
			return {
				...state,
				step: action.payload,
			};

		case "upload_bukti":
			return {
				...state,
				step: action.payload,
			};

		case "bayar":
			return {
				...state,
				step: action.payload,
			};

		default:
			return {
				...state,
			};
	}
};
