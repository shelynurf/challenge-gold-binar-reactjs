import React from "react";
// import { DateRangePicker } from "rsuite";
// import DateRangePicker from "rsuite/DateRangePicker";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateRangePicker } from "@mui/x-date-pickers-pro";
import { SingleInputDateRangeField } from "@mui/x-date-pickers-pro/SingleInputDateRangeField";

const CalendarMUI = ({ ...props }) => {
	return (
		<LocalizationProvider dateAdapter={AdapterDayjs}>
			{/* <DemoContainer components={["DateRangePicker"]}>
				<DemoItem
					label="Tentukan lama sewa mobil (max. 7 hari)"
					component="DateRangePicker">
					<DateRangePicker calendars={1} />
				</DemoItem>
			</DemoContainer> */}
			<DemoContainer components={["SingleInputDateRangeField"]}>
				<DateRangePicker
					label="Pilih tanggal mulai dan tanggal akhir sewa"
					slots={{ field: SingleInputDateRangeField }}
					slotProps={{
						textField: {
							size: "small",
						},
					}}
					calendars={1}
				/>
			</DemoContainer>
		</LocalizationProvider>
		// <>
		// 	<label className="font-12-thin">
		// 		Tentukan lama sewa mobil (max. 7 hari)
		// 	</label>
		// 	<DateRangePicker
		// 		{...props}
		// 		placeholder="Pilih tanggal mulai dan tanggal akhir sewa"
		// 		// showOneCalendar
		// 		// disabledDate={allowedMaxDays(8)}
		// 		block
		// 		// value={value}
		// 		// onChange={setValue}
		// 		format="dd-MMM-yyyy"
		// 	/>
		// </>
	);
};

export default CalendarMUI;
