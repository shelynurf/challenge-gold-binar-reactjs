import React from "react";
import { useCountdown } from "./countdown";
import DateTimeDisplay from "./dateTimeDisplay";

const ShowCounter = ({ hours, minutes, seconds }) => {
	return (
		<div className="d-flex gap-2">
			{hours > 0 && (
				<div className="d-flex gap-2">
					<DateTimeDisplay value={hours} isDanger={false} />
					<p>:</p>
				</div>
			)}

			<DateTimeDisplay value={minutes} isDanger={false} />
			<p>:</p>
			<DateTimeDisplay value={seconds} isDanger={false} />
		</div>
	);
};

const CountdownTimer = ({ targetDate }) => {
	const [hours, minutes, seconds] = useCountdown(targetDate);
	if (hours + minutes + seconds <= 0) {
		return <div>Times Up</div>;
	} else {
		return <ShowCounter hours={hours} minutes={minutes} seconds={seconds} />;
	}
};

export default CountdownTimer;
