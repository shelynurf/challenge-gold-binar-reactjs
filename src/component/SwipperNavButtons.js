import React from "react";
import { useSwiper } from "swiper/react";

export const SwiperNavButtons = () => {
	const swiper = useSwiper();
	return (
		// <div
		// 	className="glide__arrows d-flex gap-3 justify-content-center"
		// 	data-glide-el="controls">
		// 	<button
		// 		className="testi-arrow testi-arrow-left"
		// 		data-glide-dir="<"
		// 		onClick={() => swiper.slidePrev()}>
		// 		<span aria-hidden="true">&lt;</span>
		// 	</button>
		// 	<button
		// 		className="testi-arrow testi-arrow-right"
		// 		data-glide-dir=">"
		// 		onClick={() => swiper.slideNext()}>
		// 		<span aria-hidden="true">&gt;</span>
		// 	</button>
		// </div>
		<div className="d-flex justify-content-center gap-3 mt-4">
			<button className="nav-prev" onClick={() => swiper.slidePrev()}>
				<i class="fa-solid fa-chevron-left"></i>
			</button>
			<button className="nav-next" onClick={() => swiper.slideNext()}>
				<i class="fa-solid fa-chevron-right"></i>
			</button>
		</div>
	);
};
