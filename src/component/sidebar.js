import React from "react";

const sideBar = ({open, close}) => {
    return (
        <aside id='aside-nav' className={open ? "d-flex" : "d-none"}>
        <div>
            <div>
                <strong>BCR</strong>
                <span>X</span>
            </div>
            <ul>
                <li>Our Service</li>
                <li>Why Us</li>
                <li>Testimonial</li>
                <li>FAQ</li>
            </ul>
        </div>

    </aside>
    )
}

export default sideBar