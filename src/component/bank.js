import BCALogo from "../assets/images/bca-logo.png";
import BNILogo from "../assets/images/bni-logo.png";
import MandiriLogo from "../assets/images/mandiri-logo.png";

export const Bank = [
	{
		name: "BCA",
		bankImg: BCALogo,
		metodeBayar: [
			{
				metode: "ATM BCA",
				instruksi: [
					"Masukkan kartu ATM, lalu PIN",
					`Pilih menu “Transaksi Lainnya” – ‘Transfer” – “Ke Rek BCA Virtual
				Account”`,
					"Masukkan nomor BCA Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar ATM akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Ambil dan simpanlah bukti transaksi tersebut",
				],
			},
			{
				metode: "MBCA",
				instruksi: [
					"Pilih m-Transfer - BCA Virtual Account",
					"Masukkan nomor BCA Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Masukkan pin m-BCA",
					"Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran",
				],
			},
			{
				metode: "BCA Klik",
				instruksi: [
					"Masukkan User ID dan PIN",
					"Pilih Transfer Dana > Transfer ke BCA Virtual Account",
					`Pilih menu “Transaksi Lainnya” – ‘Transfer” – “Ke Rek BCA Virtual
				Account”`,
					"Masukkan nomor BCA Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Validasi pembayaran",
					"Cetak nomor referensi sebagai bukti transaksi Anda",
				],
			},
			{
				metode: "Internet Banking",
				instruksi: [
					"Masukkan User ID dan password",
					`Pilih menu ‘Transfer” – “Ke Rek BCA Virtual
				Account”`,
					"Masukkan nomor BCA Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar ATM akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Masukkan Kode Otentikasi Token",
					"Anda akan menerima notifikasi bahwa transaksi berhasil",
				],
			},
		],
	},

	{
		name: "BNI",
		bankImg: BNILogo,
		metodeBayar: [
			{
				metode: "ATM BNI",
				instruksi: [
					"Masukkan kartu ATM, lalu PIN",
					`Pilih menu “Transaksi Lainnya” – ‘Transfer” – “Ke Rek BNI Virtual
				Account”`,
					"Masukkan nomor BNI Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar ATM akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Ambil dan simpanlah bukti transaksi tersebut",
				],
			},
			{
				metode: "BNI Mobile",
				instruksi: [
					"Pilih m-Transfer - BNI Virtual Account",
					"Masukkan nomor BNI Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Masukkan pin BNI Mobile",
					"Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran",
				],
			},
			{
				metode: "SMS Banking",
				instruksi: [
					"Buka aplikasi SMS Banking BNI",
					`Pilih menu – ‘Transfer” – “Ke Rek BNI Virtual
				Account”`,
					"Masukkan nomor BNI Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar ATM akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Pilih Proses, kemudian Setuju",
					"Balas sms dengan mengetik pin sesuai dengan instruksi BNI",
					"Anda akan menerima notif bahwa transaksi berhasil",
				],
			},
			{
				metode: "Internet Banking",
				instruksi: [
					"Masukkan User ID dan password",
					`Pilih menu ‘Transfer” – “Ke Rek BNI Virtual
				Account”`,
					"Masukkan nomor BNI Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar ATM akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Masukkan Kode Otentikasi Token",
					"Anda akan menerima notifikasi bahwa transaksi berhasil",
				],
			},
		],
	},

	{
		name: "Mandiri",
		bankImg: MandiriLogo,
		metodeBayar: [
			{
				metode: "ATM Mandiri",
				instruksi: [
					"Masukkan kartu ATM, lalu PIN",
					`Pilih menu “Transaksi Lainnya” – ‘Transfer” – “Ke Rek Mandiri Virtual
				Account”`,
					"Masukkan nomor Mandiri Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar ATM akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Ambil dan simpanlah bukti transaksi tersebut",
				],
			},
			{
				metode: "Livin Mandiri",
				instruksi: [
					"Pilih m-Transfer - Mandiri Virtual Account",
					"Masukkan nomor Mandiri Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Masukkan pin Livin Mandiri",
					"Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran",
				],
			},

			{
				metode: "Internet Banking",
				instruksi: [
					"Masukkan User ID dan password",
					`Pilih menu ‘Transfer” – “Ke Rek Mandiri Virtual
				Account”`,
					"Masukkan nomor Mandiri Virtual Account: 70020+Order ID",
					"Contoh:",
					"No. Peserta: 12345678, maka ditulis 7002012345678",
					"Layar ATM akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi",
					"Masukkan Kode Otentikasi Token",
					"Anda akan menerima notifikasi bahwa transaksi berhasil",
				],
			},
		],
	},
];
