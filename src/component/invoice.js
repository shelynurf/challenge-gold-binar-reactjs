import React, { useCallback, useEffect, useRef, useState } from "react";
import { fetchApi } from "../config/services";
import { useParams } from "react-router-dom";
// import uuid from "react-uuid";
import { v4 as uuid } from "uuid";
import { useSelector } from "react-redux";
import lunasPic from "../assets/images/lunas.png";

const Invoice = React.forwardRef((props, componentRef) => {
	const [data, setData] = useState(null);
	const { id } = useParams();
	const fetchingMobil = useCallback(
		(params = null) => {
			fetchApi(
				`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`,
				params
			).then((result) => {
				// console.log("result :", result);
				setData(result.data);
			});
		},
		[id]
	);

	useEffect(() => {
		fetchingMobil();
	}, [fetchingMobil]);

	const formatNumber = (number) =>
		new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		})
			.format(number)
			.slice(0, -3);

	// const invNumber = () => {
	// 	const invRandom = () => {
	// 		return Math.floor(Math.random() * 100000);
	// 	};
	// 	const invUUID = () => {
	// 		return uuid().slice(0, 7);
	// 	};
	// 	const date = new Date().toJSON().slice(0, 10).replace(/-/g, "");
	// 	// console.log("typeof invRandom", typeof invRandom);
	// 	return `${invRandom()} / ${invUUID()} / ${date};`;
	// };
	const orderStart = useSelector((state) => state?.orderDate?.start);
	const orderEnd = useSelector((state) => state?.orderDate?.end);
	const countOrderDate = useSelector((state) => state?.orderDate?.countOrder);
	const totalPrice = useSelector((state) => state?.orderDate?.totalPrice);
	const orderID = useSelector((state) => state?.orderDate?.orderID);
	const invNumber = useSelector((state) => state?.orderDate?.invoice);
	const orderDate = new Date().toLocaleDateString("in-ID", {
		day: "numeric",
		month: "long",
		year: "numeric",
	});

	return (
		<div className="d-flex justify-content-center" ref={componentRef}>
			<div
				style={{
					width: "80%",
					padding: "2rem",
				}}>
				<div className="d-flex justify-content-center">
					<h1 className="mb-5">INVOICE</h1>
				</div>
				<div className="text-start d-flex">
					<table style={{ width: "100%" }}>
						<tr>
							<td>Order ID</td>
							<td>:</td>
							<td>{orderID}</td>
						</tr>
						<tr>
							<td>No. Invoice</td>
							<td>:</td>
							<td>{invNumber}</td>
						</tr>
						<tr>
							<td>Tanggal Order</td>
							<td>:</td>
							<td>{orderDate}</td>
						</tr>
						<tr>
							<td>Periode Sewa</td>
							<td>:</td>
							<td>{`${orderStart?.toLocaleDateString("in-ID", {
								day: "numeric",
								month: "long",
								year: "numeric",
							})} - ${orderEnd?.toLocaleDateString("in-ID", {
								day: "numeric",
								month: "long",
								year: "numeric",
							})}`}</td>
						</tr>
					</table>
					<div>
						<img src={lunasPic} style={{ maxWidth: "10rem" }}></img>
					</div>
					{/* <p>No. Invoice : {invNumber()}</p>
					<p>Tanggal Order : {orderDate}</p>
					<p>
						Periode Sewa :{" "}
						{`${orderStart.toLocaleDateString("in-ID", {
							day: "numeric",
							month: "long",
							year: "numeric",
						})} - ${orderEnd.toLocaleDateString("in-ID", {
							day: "numeric",
							month: "long",
							year: "numeric",
						})}`}
					</p> */}
				</div>
				<div style={{ marginTop: "4rem" }}>
					<table
						className="tabel"
						style={{ width: "100%", borderCollapse: "collapse" }}>
						<tr>
							<th className="tabel tabel-header">Nama/Tipe Mobil</th>
							<th className="tabel tabel-header">Harga Sewa</th>
							<th className="tabel tabel-header">Lama Sewa</th>
							<th className="tabel tabel-header">Total Harga</th>
						</tr>
						<tr>
							<td className="tabel">{data?.name}</td>
							<td className="tabel">{formatNumber(data?.price)}</td>
							<td className="tabel">{countOrderDate} hari</td>
							<td className="tabel">{formatNumber(totalPrice)}</td>
						</tr>
					</table>
				</div>
				<div className="mt-5 text-end">
					<p>Ttd</p>
					<p>Binar Car Rental</p>
				</div>
			</div>
		</div>
	);
});

export default Invoice;
