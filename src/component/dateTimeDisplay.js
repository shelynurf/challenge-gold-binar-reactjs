import React from "react";

const DateTimeDisplay = ({ value, type, isDanger }) => {
  return (
    <div className={isDanger ? "countdown danger" : "countdown"}>
      <p
        className="font-18 d-flex justify-content-center"
        style={{
          color: "white",
          backgroundColor: "#FA2C5A",
          borderRadius: "2px",
          width: "29px",
        }}
      >
        {value}
      </p>
      <span>{type}</span>
    </div>
  );
};

export default DateTimeDisplay;
