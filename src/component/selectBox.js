import React, { useState } from "react";

const SelectBox = ({ label, data, placeholder, ...props }) => {
	const [expand, setExpand] = useState(false);
	return (
		<div class="col">
			<label class="form-label">{label}</label>
			<select
				class="form-control"
				onClick={() => setExpand(!expand)}
				style={{ color: "#8A8A8A" }}
				{...props}>
				<i
					className={`fa-solid  ${
						expand ? "fa-chevron-up" : "fa-chevron-down"
					}`}></i>
				<option selected disabled>
					{placeholder}
				</option>
				{data?.map((item, index) => (
					<option key={index} value={item.value}>
						{item.label}
					</option>
				))}
			</select>
		</div>
	);
};

export default SelectBox;
