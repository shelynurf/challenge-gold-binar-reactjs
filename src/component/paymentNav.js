import React from "react";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";

const PaymentNav = ({ payNav, ...props }) => {
	const navigate = useNavigate();
	const state = useSelector((state) => state.stepper.step);
	const orderID = useSelector((state) => state?.orderDate?.orderID);
	// console.log("state", state);

	return (
		<div className=" container d-flex justify-content-between">
			<div>
				<div className="d-flex gap-3 align-items-baseline">
					<button
						onClick={() => navigate(-1)}
						style={{ border: "none", backgroundColor: "#f1f3ff" }}>
						<i className="fa-solid fa-arrow-left mt-1"></i>
					</button>
					<div>
						<p className="font-14 mb-0">{payNav}</p>
						{!state.includes("pilih") && (
							<p className="font-12">Order ID : {orderID}</p>
						)}
					</div>
				</div>
			</div>

			<div>
				<div className="d-flex gap-3 align-items-center">
					<div className="d-flex gap-2">
						{state.includes("pilih") && (
							<span className="mb-0 font-10 bullet-fill">1</span>
						)}
						{state.includes("pilih-selesai") && (
							<i className="fa-solid fa-check fa-2xs bullet-fill"></i>
						)}
						<span className="mb-0 font-12">Pilih metode</span>
					</div>
					<div>
						<hr className="line"></hr>
					</div>
					<div className="d-flex gap-2">
						{state.includes("pilih") && (
							<span className="font-10 bullet-outline">2</span>
						)}
						{state.includes("upload") && (
							<span className="mb-0 font-10 bullet-fill">2</span>
						)}
						{state.includes("upload-selesai") && (
							<i className="fa-solid fa-check fa-2xs bullet-fill"></i>
						)}
						<span className="font-12 mb-0">Bayar</span>
					</div>
					<div>
						<hr className="line"></hr>
					</div>
					<div className="d-flex gap-2">
						{state.includes("pilih") && (
							<span className="font-10 bullet-outline">3</span>
						)}
						{state.includes("upload") && (
							<span className="font-10 bullet-outline">3</span>
						)}
						{state.includes("bayar") && (
							<span className="mb-0 font-10 bullet-fill">3</span>
						)}
						{state.includes("bayar-selesai") && (
							<i className="fa-solid fa-check fa-2xs bullet-fill"></i>
						)}
						<span className="font-12 mb-0">Tiket</span>
					</div>
				</div>
			</div>
		</div>
	);
};

export default PaymentNav;
