import React from "react";

const Input = ({ label, ...props }) => {
	return (
		<div className="col">
			<label className="form-label">{label}</label>
			<input
				className="form-control"
				style={{ color: "#8A8A8A" }}
				{...props}></input>
		</div>
	);
};

export default Input;
