import { addDays } from "date-fns";
import React, { useCallback, useEffect, useState } from "react";
import { DateRange, DateRangePicker } from "react-date-range";

import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { fetchApi } from "../config/services";

const Calendar = () => {
	const [orderDate, setOrderDate] = useState([
		{
			startDate: new Date(),
			endDate: new Date(),
			key: "selection",
		},
	]);

	const [data, setData] = useState(null);
	const { id } = useParams();
	const fetchingMobil = useCallback(
		(params = null) => {
			fetchApi(
				// `https://api-car-rental.binaracademy.org/customer/car/${id}`,
				`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`,

				params
			)
				.then((result) => {
					// console.log("result :", result);
					setData(result.data);
				})
				.catch((e) => {});
		},
		[id]
	);

	useEffect(() => {
		fetchingMobil();
	}, [fetchingMobil]);

	const [maxDate, setMaxDate] = useState(null);
	// const maxOrderDate = addDays(new Date(state[0].startDate), 6);
	const handleChange = (item, state) => {
		setOrderDate([item["selection"]]);
		setMaxDate(addDays(new Date(item["selection"].startDate), 6));

		// setState({ ...state, ...item });
	};
	const orderStart = orderDate[0].startDate;
	const orderEnd = orderDate[0].endDate;
	const countOrder =
		(orderDate[0].endDate.getTime() - orderDate[0].startDate.getTime()) /
			(1000 * 60 * 60 * 24) +
		1;

	const totalPrice = data?.price * countOrder;
	// console.log("corder start", orderDate[0].startDate);

	// console.log("start :", orderDate[0].startDate);
	// console.log("data", data);
	// console.log("maxOrderDate :", maxOrderDate);
	// const selectionRange = {
	// 	startDate: new Date(),
	// 	endDate: new Date(),
	// 	key: "selection",
	// };
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch({
			type: "order-start",
			payload: orderStart,
		});
		dispatch({
			type: "order-end",
			payload: orderEnd,
		});
		dispatch({
			type: "count-order-date",
			payload: countOrder,
		});
		dispatch({
			type: "total-price",
			payload: totalPrice,
		});
	});
	return (
		<DateRange
			editableDateInputs={true}
			onChange={handleChange}
			moveRangeOnFirstSelection={false}
			ranges={orderDate}
			maxDate={maxDate}
			minDate={addDays(new Date(), 0)}
		/>
	);
	// <DateRangePicker ranges={[selectionRange]} onChange={handleSelect} />;
};

export default Calendar;
